# Tokiwen

Software development environment to assist in the study of basic computer programming and computer architectures.
Available (hopefully) at <https://tokiwen.veigo.dev/>.

Developed for the Análise e Desenvolvimento de Sistemas technologist degree at IFRS Campus Rio Grande.
